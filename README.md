# Drone Commands Interpreter

O objetivo desta função lambda é interpretar os comandos enviados através do skill da alexa e implementar um modelo de interação com o usuário através de [Intents e Slots](https://developer.amazon.com/en-US/docs/alexa/custom-skills/create-intents-utterances-and-slots.html), e assim disparar um evento no serviço de fila [SQS](https://aws.amazon.com/sqs/), representando um comando para a interação com o drone. A função também grava o comando em um arquivo no serviço de storage [S3](https://aws.amazon.com/s3/).

## Configuração do Ambiente

### Simple Storage Service (S3)

Para que o arquivos html e txt sejam gerados no bucket S3, deve ser realizada a criação prévia do bucket onde serão armazenados os arquivos de saída da função. As imagens a seguir ilustrão o processo de criação no console da AWS:

Abra o console da AWS e selecione o serviço do S3:
![01-select-s3](./docs/images/s3/01-select-s3.png)

Selecione a opção para criar um novo bucket:
![02-create-bucket](./docs/images/s3/02-create-bucket.png)

Preencha as informações de nomeclatura e região de disponibilidade de acordo com suas preferências e clique no botão `Create`:
![03-perform-create](./docs/images/s3/03-perform-create.png)

Verifique a criação do bucket e copie o identificador do bucket (ARN) e guardê-o para posterior configuração de deploy da lambda.
![04-checkcreation](./docs/images/s3/04-checkcreation.png)


### Simple Queue Service (SQS)

Para que o comando seja encaminhado para API e haja a garantia de recebimento das mensagens em ordem pela API de interface com o drone, a função lambda envia os comandos interpretados para uma fila FIFO no SQS. Com o objetivo de ter visibilidade de falhas no envio de comandos e pós-processamento de tais falhas, também tem-se a necessidade de configurar uma fila chamada *Dead Letter Queue*(DLQ). Assim, a cada X número de falhas (configurado no parâmetro `Maximum receives` no momento de criação da fila) no processamento de tal recebimento, a mensagem é redirecionada para a DLQ. A seguir demonstra-se um exemplo de configuração para teste do experimento com o drone.

Abra o console da AWS e selecione o serviço do SQS:
![01-select-service.png](./docs/images/sqs/01-select-service.png)

Selecione a opção para criar primeiramente a DLQ:
![02-select-createqueue](./docs/images/sqs/02-select-createqueue.png)

Seleciona a opção para criar uma fila FIFO (First In First Out):
![03-select-fifo](./docs/images/sqs/03-select-fifo.png)

Especifique os parâmetros da fila de acordo com a necessidade de retries e nomeie a fila de acordo com o padrão fifo:
![04-specify-queue-params](./docs/images/sqs/04-specify-queue-params.png)

Selecione as configurações básicas de permissão da fila:
![05-select-basic](./docs/images/sqs/05-select-basic.png)

Clique em `Create` para criar a fila:
![06-create-queue](./docs/images/sqs/06-create-queue.png)

Verifique a criação da fila, e crie uma nova fila do tipo FIFO, que irá receber as mensagens:
![07-check-creation](./docs/images/sqs/07-check-creation.png)

Especifique os parâmetros da fila de acordo com a necessidade de retries e nomeie a fila de acordo com o padrão fifo:
![08-create-regular](./docs/images/sqs/08-create-regular.png)

Configure a fila para encaminhar as mensagens com falha no processamento para serem reencaminhadas á DLQ previamente criada: 
![09-select-dlq](./docs/images/sqs/09-select-dlq.png)

Verifique a criação da fila FIFO, copie e guarde os valores do ARN e QUEUE URL para configuração das variáveis de ambiente da lambda
![10-check-creation](./docs/images/sqs/10-check-creation.png)

Selecione nas configurações da fila, a opção política de acesso:
![11-select-queue-access-policy](./docs/images/sqs/11-select-queue-access-policy.png)

Em seguida, altere as permissões para que as lambdas tenham acesso para enviar e receber mensagens da fila:
![12-change-permission](./docs/images/sqs/12-change-permission.png)

Assim, o mecânismo de retentativas e *Dead Letter Queue* estão configurados para serem utilizados pelas funções lambda.

## Instalação

As instruções abaixo foram testadas em sistemas operacionais baseados em UNIX (Linux e MacOS).

#### [Pré-requisitos](#prereq)
- Conta da AWS - preferêncialmente com Administrator Access para teste, porém não recomendado em ambiente produtivo
- Configuração das filas no Simple Queue Service (SQS) na AWS
- Instalação e Configuração da [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- Instalação do [Git](https://git-scm.com/downloads)
- Instalação do [Nodejs 12+](https://nodejs.org/en)
- Instalação do [Serverless Framework](https://www.serverless.com/framework/docs/getting-started/)⚡
- Instalação do [Python 3.7](https://www.python.org/downloads/)
- Instalação do [Pipenv](https://pipenv-fork.readthedocs.io/en/latest/)

Em uma shell de terminal, navegue até o diretório que deseja realizar a instalação e clone o repositório:

```sh
git clone https://ggleme@bitbucket.org/ggleme/drone-commands-interpreter.git

cd drone-commands-interpreter

cp sample.env .env
```

Instale as depedências do projeto:

```sh
npm install

pipenv install
```

Abra o arquivo .env e altere as configurações de acordo com as configurações da fila criada no serviço SQS da AWS. Segue um exemplo abaixo:

```.env
# Configurações da skill
export AWS_ALEXA_SKILL_ID=amzn1.ask.skill.c1b3d932-d198-4251-9f00-e64ea6eff3c7

# Configurações do Bucket S3
export AWS_S3_BUCKET_NAME=iotce289
export AWS_S3_BUCKET_ROOT_PATH=alexa/
export AWS_S3_EVENTS_BUCKET_ARN=arn:aws:s3:sa-east-1:233315128627:iotce289

# Configurações da Fila
export AWS_SQS_QUEUE_URL=https://sqs.sa-east-1.amazonaws.com/231116128697/iot-alexa-events.fifo
export AWS_SQS_COMMANDS_EVENTS_QUEUE_ARN=arn:aws:sqs:sa-east-1:233315128627:iot-alexa-events.fifo

# Opção para habilitar os logs de debug no deploy
export SLS_DEBUG=*
```

Em seguida, exporte as variáveis de ambiente rodando o seguinte comando:

```sh
source .env
```

# Deploy

Após ter configurado a AWS CLI, é possível realizar o deploy através do comando `serverless deploy -v`.

Para realizar o deploy em outro perfil da AWS (caso tenha múltiplos perfis), selecione o perfil através da variável de ambiente `AWS_PROFILE`, sendo assim, o comando de deploy fica `AWS_PROFILE=meu-outro-perfil serverless deploy -v`.

Após executar o comando, o projeto será configurado automaticamente em sua conta da AWS através dos scripts de [CloudFormation](https://aws.amazon.com/cloudformation/) gerados pelo **Serverless Framework**⚡.

Para verificar o deploy da função lambda, navegue no serviço Lambda no Console da AWS, na região South America e verifique que a função `drone-commands-interpreter-dev-main` foi criada com sucesso. 

Também é possível verificar a nova função através do comando `aws lambda list-functions`, que retornará as informações de todas as funções disponíveis na conta configurada na AWS CLI.

**Observação:** caso deseje alterar a região de deploy da função, é possível realizar tal configuração alterando a linha 8 do arquivo `serverless.yml` de acordo com o código da região que deseja, por exemplo, *us-east-1*, que representa a região US North Virginia na AWS.


# Troubleshooting

- Caso o comando de instalação das dependências falhe, garanta que:
  - A instalação do Node.js está apontada para a versão correta, assim, o comando `node -v` deveria retornar a versão correta da runtime do Node.js
  - A instalação do Python e Pipenv estão corretas, assim o comando `python --version` deveria retornar a versão correnta da runtime do Python especificada nos [Pré-requisitos](#prereq)

- Caso o comando de deploy falhe garanta que:
  - As variáveis de ambiente estão configuradas corretamente
  - A conta configurada na CLI da AWS tem permissões para criar e configurar todos os recursos necessários na criação da Stack do CloudFormation. Para ajudar na criação de uma política de segurança (caso o acesso administrador seja restrito) utilize o gerador de políticas básicas disponibilizado pelo [generator-serverless-policy](https://github.com/dancrumb/generator-serverless-policy). 

Abaixo temos um exemplo de politicas, com permissões mais abertas, que permitem a realização do deploy das funções lambdas do projeto.

![policy](./docs/images/policy.png)