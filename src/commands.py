import json
from services import s3, sqs
import helpers


def get_welcome_response():
    """
    Welcome message
    """
    session_attributes = {}
    card_title = 'Welcome'
    speech_output = 'Conectando-se ao drone. Diga se deseja iniciar, parar ' \
                    'ou interromper uma inspeção em andamento.'
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = 'Você pode controlar o drone dizendo: iniciar inspeção, ' \
                    'parar inspeção, ou interromper inspeção.'
    should_end_session = False
    speechlet_res = helpers.build_speechlet_response(
        card_title, speech_output,
        reprompt_text,
        should_end_session
    )
    return helpers.build_response(session_attributes, speechlet_res)

def handle_session_end_request():
    """
    Session ended handler 
    """
    card_title = 'Session Ended'
    speech_output = 'Desconectar do drone. Feito!'
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    speechlet_res = helpers.build_speechlet_response(
        card_title,
        speech_output,
        None,
        should_end_session
    )
    return helpers.build_response({}, speechlet_res)


def build_command_file_contents(command: str, format: str = 'text') -> str:
    """
    Builds Command File String
    """
    if format == 'text':
        return command.encode('utf-8')
    elif format == 'html':
        return f'<html><body>{command}</body></html>'.encode('utf-8')
    else:
        raise ValueError('format not supported')


def store_command_file(command: str, filename: str, format: str = 'text'):
    """
    Store command file contents on the configured s3 bucket
    """
    file_content = build_command_file_contents(command, format=format)
    content_type = ''
    if format == 'text':
        content_type = 'text/plain'
    elif format == 'html':
        content_type =  'text/html'
    
    return s3.put_object(filename, file_content, content_type)


def build_command_message(command: str, user_id: str, request_id: str):
    """
    Builds command message for SQS
    """
    message_body = {
        'command': command,
        'userId': user_id,
        'requestId': request_id,
    }
    return json.dumps(message_body)


def send_command_message(command: str, intent_name: str, user_id: str, request_id: str):
    """
    Sends drone command to the command bus
    """
    message = build_command_message(command, user_id, request_id)
    return sqs.send_message(message, intent_name, request_id)

def dispatch_drone_command(request, intent, session):
    """
    Interpreta comandos de voz e envia para o drone via S3 e SQS
    Comandos implementados: INTERROMPER, PARAR, INICIAR
    """
    
    session_attributes = {}
    should_end_session = False
    speech_output = ''
    reprompt_text = ''
    command = None
    
    if 'Comando' in intent['slots']:
        command = str(intent['slots']['Comando']['resolutions']['resolutionsPerAuthority'][0]['values'][0]['value']['id'])
        reprompt_text = f'Enviando o comando {command} para o drone'
        speech_output = reprompt_text
        session_attributes = {'ultimoComando': command}
        store_command_file(command, filename='comando.txt', format='text')
        store_command_file(command, filename='comando.html', format='html')
        send_command_message(command, intent['name'], session['user']['userId'], request['requestId'])
        
    else:
        speech_output = 'Repita por favor, não entendi o comando'
        reprompt_text = 'Não tenho certeza. Por exemplo diga iniciar inspeção ou parar inspeção.'
    
    speechlet_res = helpers.build_speechlet_response(
        intent['name'],
        speech_output,
        reprompt_text,
        should_end_session
    )
    return helpers.build_response(session_attributes, speechlet_res)