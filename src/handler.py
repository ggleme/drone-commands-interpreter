import json
import events, settings


def execute(event, context):
    """ 
    Route the incoming request based on type (LaunchRequest, IntentRequest, etc.)
    The JSON body of the request is provided in the event parameter.
    """
    print(f"event.session.application.applicationId={event['session']['application']['applicationId']}")

    """
    Prevents someone else from configuring a skill that sends requests to this function.
    """
    request = event['request']

    if event['session']['application']['applicationId'] != settings.AWS['alexaSkill'].get('skill_id'):
       raise ValueError('Invalid Application ID')

    if event['session']['new']:
        events.on_session_started({'requestId': request['requestId']}, event['session'])
    
    if request['type'] == 'LaunchRequest':
        return events.on_launch(request, event['session'])
    
    elif request['type'] == 'IntentRequest':
        return events.on_intent(request, event['session'])

    elif request['type'] == 'SessionEndedRequest':
        return events.on_session_ended(request, event['session'])

