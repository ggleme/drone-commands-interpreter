import os

AWS = {
  's3': {
    'bucket_name': os.getenv('AWS_S3_BUCKET_NAME'),
    'root_path': os.getenv('AWS_S3_BUCKET_ROOT_PATH'),
  },
  'sqs': {
    'queue_url': os.getenv('AWS_SQS_QUEUE_URL'),
  },
  'alexaSkill': {
    'skill_id': os.getenv('AWS_ALEXA_SKILL_ID'),
  }
}
