import settings
import boto3


s3 = boto3.resource('s3')
bucket = s3.Bucket(settings.AWS['s3'].get('bucket_name'))
root_path = settings.AWS['s3'].get('root_path')

def put_object(filename: str, body: str, content_type: str = 'text/plain'):
  """Put object (file) on the configured s3 bucket"""
  filepath = root_path + filename
  return bucket.put_object(
    Key=filepath,
    Body=body,
    ACL='public-read',
    ContentType=content_type,
  )
