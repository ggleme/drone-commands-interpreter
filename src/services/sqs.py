import boto3
import settings

sqs = boto3.client('sqs')
queue_url = settings.AWS['sqs'].get('queue_url')

def send_message(message: str, group_id: str, deduplication_id: str):
  """
  Sends a formatted message to the queue
  """
  print('deduplition_id', deduplication_id)
  return sqs.send_message(
    QueueUrl=queue_url,
    MessageBody=message,
    MessageGroupId=group_id,
    MessageDeduplicationId=deduplication_id,
  )

