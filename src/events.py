import commands

def on_session_started(session_started_request, session):
    """
    Called when the session starts
    """
    print(
      f"on_session_started requestId={session_started_request['requestId']}, " +
      f"sessionId={session['sessionId']}"
    )
    

def on_launch(launch_request, session):
    """
    Called when the user launches the skill without specifying what they want
    """
    print(
      f"on_launch requestId={launch_request['requestId']}, " +
      f"sessionId={session['sessionId']}"
    )
    # Dispatch to your skill's launch
    return commands.get_welcome_response()


def on_session_ended(session_ended_request, session):
    """
    Called when the user ends the session.
    Is not called when the skill returns should_end_session=true
    """
    print(
        f"on_session_ended requestId={session_ended_request['requestId']}, " +
        f"sessionId={session['sessionId']}"
    )
    # add cleanup logic here


def on_intent(request, session):
    """ Called when the user specifies an intent for this skill """
    print(
        f"on_intent requestId={request['requestId']}, " +
        f"sessionId={session['sessionId']}"
    )

    intent = request['intent']
    intent_name = request['intent']['name']

    # Dispatch to your skill's intent handlers
    
    if intent_name == "DroneIntent":
        return commands.dispatch_drone_command(request, intent, session)

    elif intent_name == "AMAZON.HelpIntent":
        return commands.get_welcome_response()

    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return commands.handle_session_end_request()
        
    else:
        raise ValueError("Invalid intent")
